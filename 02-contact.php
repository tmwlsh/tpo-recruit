<?php
/**
 * Template Name: 02 - Contact
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>

	<div class="contact-page">

		<div class="page-header">
			<div class="container">
				<h1>Contact Us</h1>
			</div>
		</div>

		<div class="contact-columns">
			<div class="container clearfix">
				<div class="sidebar">
					<?php $contact = get_field('contact', 'options'); ?>
					<div class="sidebar-row">
						<p>Call Us:</p>
						<h4><a href="tel:<?php echo $contact['phone_number']; ?>"><?php echo $contact['phone_number']; ?></a></h4>
					</div>
					<div class="sidebar-row">
						<p>Email Us:</p>
						<h4><a href="mailto:<?php echo $contact['email_address']; ?>"><?php echo $contact['email_address']; ?></a></h4>
					</div>
					<div class="sidebar-row">
						<p>Find Us:</p>
						<h4><?php echo $contact['address']; ?></h4>
					</div>
					<?php include(get_stylesheet_directory() . '/img/secondary-logo.svg'); ?>
					<?php unset($contact); ?>
				</div>
				<div class="form">
					<?php echo do_shortcode('[contact-form-7 id="7" title="Contact form 1"]'); ?>
					<?php include(get_stylesheet_directory() . '/img/secondary-logo.svg'); ?>
				</div>
			</div>
		</div>


	</div>

<?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer' ) ); ?>