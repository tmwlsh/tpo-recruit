<footer>

	<div class="container clearfix">
		<div class="column menu logo">
			<?php include(get_stylesheet_directory() . '/img/secondary-logo.svg'); ?>
			<?php include(get_stylesheet_directory() . '/img/main-logo.svg'); ?>
		</div>
		<div class="column menu">
			<nav><?php wp_nav_menu( array( 'container_class' => 'footer-nav', 'theme_location' => 'footer-left' ) ); ?></nav>
		</div>
		<div class="column menu">
			<nav><?php wp_nav_menu( array( 'container_class' => 'footer-nav', 'theme_location' => 'footer-right' ) ); ?></nav>
		</div>
		<div class="column menu social">
			<?php $social = get_field('social', 'options'); ?>
			<?php if($social['linkedin']): ?>
				<a target="_blank" href="<?php echo $social['linkedin']; ?>"><?php include(get_stylesheet_directory() . '/img/linkedin-brands.svg'); ?></a>
			<?php endif; ?>
			<?php if($social['instagram']): ?>
				<a target="_blank" href="<?php echo $social['instagram']; ?>"><?php include(get_stylesheet_directory() . '/img/instagram-brands.svg'); ?></a>
			<?php endif; ?>
		</div>
	</div>

	<div class="copyright">
		<div class="container clearfix">
			<div class="content">
				<p>&copy; The People Of <?php echo get_the_date('Y'); ?></p>
			</div>
			<div class="content">
				<a target="_blank" href="http://www.tmwlsh.co.uk">Site by TMWLSH</a>
			</div>
		</div>
	</div>

</footer>