<div class="header-menu">
	<nav>
		<?php wp_nav_menu( array( 'container_class' => 'primary-nav', 'theme_location' => 'header-menu' ) ); ?>
	</nav>
</div>

<?php if(is_front_page()): ?>
	<header class="front">
<?php else: ?>
	<header>
<?php endif; ?>
	<div class="container clearfix">
		<a class="logo" href="<?php echo get_home_url(); ?>"><?php include(get_stylesheet_directory() . '/img/main-logo.svg'); ?></a>
		<nav>
			<?php $contact = get_field('contact', 'options'); ?>
			<a class="contact" href="tel:<?php echo $contact['phone_number']; ?>"><?php include(get_stylesheet_directory() . '/img/phone-alt-solid.svg'); ?><?php echo $contact['phone_number']; ?></a>
			<a class="contact" href="mailto:<?php echo $contact['email_address']; ?>"><?php echo $contact['email_address']; ?></a>
			<?php wp_nav_menu( array( 'container_class' => 'primary-nav', 'theme_location' => 'header-menu' ) ); ?>
			<?php unset($contact); ?>
		</nav>
		<div class="hamb">
			<div class="line"></div>
			<div class="line"></div>
			<div class="line"></div>
		</div>
	</div>
</header>