<?php
/* Module: Content builder */
$acf_id = mbt_acf_id();
if( have_rows('content_builder_modules', $acf_id) ): ?>

    <section class="content-builder">
        <?php while ( have_rows('content_builder_modules', $acf_id) ) : the_row();

            if( get_row_layout() == 'banner_text' ) {
                get_template_part( 'modules/module', 'banner-text' );
            } elseif( get_row_layout() == 'about_header' ) {
                get_template_part( 'modules/module', 'about-header' );
            } elseif( get_row_layout() == 'title_paragraph' ) {
                get_template_part( 'modules/module', 'title-paragraph' );
            } elseif( get_row_layout() == 'full_image' ) {
                get_template_part( 'modules/module', 'full-image' );
            } elseif( get_row_layout() == 'staff_selector' ) {
                get_template_part( 'modules/module', 'staff-selector' );
            } elseif( get_row_layout() == 'block_title' ) {
                get_template_part( 'modules/module', 'block-title' );
            }

        endwhile; ?>
    </section>

<?php endif;