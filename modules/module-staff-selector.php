<?php /* Module: Staff Selector */ ?>

<div class="module module-staff-selector">
    <div class="container">

        <?php if( have_rows('staff_repeater') ): ?>
            <?php while ( have_rows('staff_repeater') ) : the_row(); ?>

                <?php $post_object = get_sub_field('staff_member'); ?>

                <?php if( $post_object ): ?>
                    <?php $post = $post_object; ?>
                    <?php setup_postdata( $post ); ?>
                        <div class="staff-row clearfix">
                            <div class="image">
                                <?php $img = get_the_post_thumbnail_url(); ?>
                                <img src="<?php echo $img; ?>" alt="<?php the_title(); ?> - The People Of" />
                                <?php include(get_stylesheet_directory() . '/img/secondary-logo.svg'); ?>
                            </div>
                            <div class="content">
                                <?php $staffInfo = get_field('staff_info'); ?>
                                <h3><?php the_title(); ?></h3>
                                <a href="mailto:<?php echo $staffInfo['staff_email_address']; ?>"><?php echo $staffInfo['staff_email_address']; ?></a><br/>
                                <a href="tel:<?php echo $staffInfo['staff_phone_number']; ?>"><?php echo $staffInfo['staff_phone_number']; ?></a><br/>
                                <a target="_blank" href="<?php echo $staffInfo['staff_linkedin']; ?>">Visit my Linkedin!</a>
                                <p><?php the_content(); ?></p>
                            </div>
                        </div>
                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>

            <?php endwhile; ?>
        <?php endif; ?>

    </div>
</div>

<?php
