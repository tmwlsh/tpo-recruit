<?php /* Module: Banner Text */ ?>

<div class="module module-about-header" style="background-image: url('<?php echo get_sub_field('about_header_image'); ?>');">
    <div class="text-block">
        <div class="container">
            <h1><?php echo get_sub_field('about_header_title'); ?></h1>
        </div>
        <?php if( have_rows('about_sub_titles') ): ?>
            <ul class="text-row clearfix">
                <?php while ( have_rows('about_sub_titles') ) : the_row(); ?>
                    <li>
                        <h4><?php the_sub_field('sub_title'); ?></h4>
                        <p><?php the_sub_field('sub_title_paragraph'); ?></p>
                    </li>
                <?php endwhile; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>

<?php
