<?php /* Module: Title Paragraph */ ?>

<div class="module module-title-paragraph">
    <div class="container clearfix">
        <div class="title-col">
            <h1><?php echo get_sub_field('title'); ?></h1>
            <?php include(get_stylesheet_directory() . '/img/secondary-logo.svg'); ?>
        </div>
        <div class="text-col">
            <h3><?php echo get_sub_field('sub_title'); ?></h3>
            <p><?php echo get_sub_field('sub_text'); ?></p>
        </div>
    </div>
</div>

<?php
