<?php /* Module: Block Title */ ?>

<div class="module module-block-title">

    <div class="container">
        <h2><?php echo get_sub_field('block_title_text'); ?></h2>
        <?php $blockLink = get_sub_field('block_title_link'); ?>
        <?php if($blockLink): ?>
        <a href="<?php echo $blockLink['url']; ?>"><?php echo $blockLink['title']; ?></a>
        <?php endif; ?>
    </div>

</div>

<?php
