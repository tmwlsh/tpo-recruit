<?php /* Module: Banner Text */ ?>

<div class="module module-banner-text" style="background-image: url('<?php echo get_sub_field('background_image'); ?>');">
    <div class="container">
        <h1><?php echo get_sub_field('text'); ?></h1>
    </div>
</div>

<?php
