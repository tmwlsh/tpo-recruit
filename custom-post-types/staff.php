<?php

	add_action( 'init', 'create_post_type_staff' );

	function create_post_type_Staff() {
		register_post_type( 'staff',
		array(
			'labels'             =>
			array(
				'name'               => _x( 'Staff', 'post type general name', 'your-plugin-textdomain' ),
				'singular_name'      => _x( 'Staff', 'post type singular name', 'your-plugin-textdomain' ),
				'menu_name'          => _x( 'Staff', 'admin menu', 'your-plugin-textdomain' ),
				'name_admin_bar'     => _x( 'Staff', 'add new on admin bar', 'your-plugin-textdomain' ),
				'add_new'            => _x( 'Add New', 'Staff', 'your-plugin-textdomain' ),
				'add_new_item'       => __( 'Add New Staff', 'your-plugin-textdomain' ),
				'new_item'           => __( 'New Staff', 'your-plugin-textdomain' ),
				'edit_item'          => __( 'Edit Staff', 'your-plugin-textdomain' ),
				'view_item'          => __( 'View Staff', 'your-plugin-textdomain' ),
				'all_items'          => __( 'All Staff', 'your-plugin-textdomain' ),
				'search_items'       => __( 'Search Staff', 'your-plugin-textdomain' ),
				'parent_item_colon'  => __( 'Parent Staff:', 'your-plugin-textdomain' ),
				'not_found'          => __( 'No Staff found.', 'your-plugin-textdomain' ),
				'not_found_in_trash' => __( 'No Staff found in Trash.', 'your-plugin-textdomain' )
			),
	        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'staff' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'menu_icon'   		 => 'dashicons-groups',
			'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
		));
	}

?>