<?php
class PT_Jobs {
	public function __construct()
	{
		// Register our post type
		add_action('init', [$this, 'create_post_type_jobs']);

		// Register our taxonomies
		add_action('init', [$this, 'register_taxonomy_location']);
		// add_action('init', [$this, 'register_taxonomy_industry']);
		add_action('init', [$this, 'register_taxonomy_salary']);
		// add_action('init', [$this, 'register_taxonomy_type']);
		add_action('init', [$this, 'register_taxonomy_category']);

		// Add our filtering AJAX call
		if (isset($_GET['ja']) && $_GET['ja'] == 1) {
			add_action('init', [$this, 'filter_jobs'], 99);
		}
	}

	public function filter_jobs()
	{
		global $wpdb;

		// Set our base query
		$args = [
			'post_type' => 'jobs',
			'tax_query' => [
				'relation' => 'AND',
			],
		];

		// Loop through taxonomies
		foreach (array('job-location', 'job-category', 'job-salary') as $taxonomy) {
			// Check if we have data submitted
			if (isset($_POST[$taxonomy])) {
				// Add our tax filter
				$args['tax_query'][] = array(
					'taxonomy' => $taxonomy,
					'field'    => 'term_id',
					'terms'    => $_POST[$taxonomy],
					'operator' => 'IN',
				);
			}
		}

		// Get our results
		$query = new WP_Query($args);

		// If we don't have results, display message
		if (!$query->have_posts()) {
			echo '<li class="filters__no-results">No results found for your search</li>';
			die;
		}

		while ($query->have_posts()) : $query->the_post();
			$jobCat = get_the_terms(get_the_ID(), 'job-category');

			$category_style = (is_wp_error($jobCat)) ? 'default' : $jobCat[0]->slug;
			?>
			<li class="<?php echo $category_style; ?>-style">
				<?php $jobInfo = get_field('job_info'); ?>
				<a href="<?php the_permalink(); ?>">
					<h3><?php echo $jobInfo['job_title']; ?></h3>
					<p><?php echo wp_trim_words( get_the_content(), 40, ' ... Read More' ); ?></p>
				</a>
				<div class="job-info">
					<div class="wage">
						<?php $jobSalary = get_the_terms(get_the_ID(), 'job-salary'); ?>
						<?php if ($jobSalary && !is_wp_error($jobSalary)) { ?>
							<p><?php echo $jobSalary[0]->name; ?></p>
						<?php } ?>
					</div>
					<div class="date">
						<p><?php echo get_the_date('d/m/y'); ?></p>
					</div>
				</div>
			</li>
		<?php
		endwhile;

		die;
	}

	public function create_post_type_jobs() {
		register_post_type( 'jobs',
		array(
			'labels'             =>
			array(
				'name'               => _x( 'Jobs', 'post type general name', 'your-plugin-textdomain' ),
				'singular_name'      => _x( 'Job', 'post type singular name', 'your-plugin-textdomain' ),
				'menu_name'          => _x( 'Jobs', 'admin menu', 'your-plugin-textdomain' ),
				'name_admin_bar'     => _x( 'Job', 'add new on admin bar', 'your-plugin-textdomain' ),
				'add_new'            => _x( 'Add New', 'job', 'your-plugin-textdomain' ),
				'add_new_item'       => __( 'Add New Job', 'your-plugin-textdomain' ),
				'new_item'           => __( 'New Job', 'your-plugin-textdomain' ),
				'edit_item'          => __( 'Edit Job', 'your-plugin-textdomain' ),
				'view_item'          => __( 'View Job', 'your-plugin-textdomain' ),
				'all_items'          => __( 'All Job', 'your-plugin-textdomain' ),
				'search_items'       => __( 'Search Jobs', 'your-plugin-textdomain' ),
				'parent_item_colon'  => __( 'Parent Jobs:', 'your-plugin-textdomain' ),
				'not_found'          => __( 'No jobs found.', 'your-plugin-textdomain' ),
				'not_found_in_trash' => __( 'No jobs found in Trash.', 'your-plugin-textdomain' )
			),
	        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'jobs' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor' )
		));
	}

	public function register_taxonomy_location()
	{
		$this->register_job_taxonomny('job-location', 'Location', 'Locations');
	}

	public function register_taxonomy_industry()
	{
		$this->register_job_taxonomny('job-industry', 'Industry', 'Industries');
	}

	public function register_taxonomy_salary()
	{
		$this->register_job_taxonomny('job-salary', 'Salary', 'Salaries');
	}

	public function register_taxonomy_type()
	{
		$this->register_job_taxonomny('job-type', 'Type', 'Types');
	}

	public function register_taxonomy_category()
	{
		$this->register_job_taxonomny('job-category', 'Discipline', 'Disciplines');
	}

	private function register_job_taxonomny($key, $single, $plural)
	{
	    register_taxonomy( $key,
	    	array('jobs'),
	    	array('hierarchical'	=> true,
	    		'labels' => array(
	    			'name'				=> __( 'Job ' . $plural, 'mblframework' ),
	    			'singular_name'		=> __( 'Job ' . $single, 'mblframework' ),
	    			'search_items'		=> __( 'Search Job ' . $plural, 'mblframework' ),
	    			'all_items'			=> __( 'All Job ' . $plural, 'mblframework' ),
	    			'parent_item'		=> __( 'Parent Job ' . $single, 'mblframework' ),
	    			'parent_item_colon'	=> __( 'Parent Job ' . $single, 'mblframework' ),
	    			'edit_item'			=> __( 'Edit Job ' . $single, 'mblframework' ),
	    			'update_item'		=> __( 'Update Job ' . $single, 'mblframework' ),
	    			'add_new_item'		=> __( 'Add New Job ' . $single, 'mblframework' ),
	    			'new_item_name'		=> __( 'New Job ' . $single, 'mblframework' )
	    		),
	    		'show_admin_column'	=> true,
	    		'show_ui'			=> true,
	    		'query_var'			=> true,
	    		'rewrite'			=> false,
	    	)
	    );
	}
}

new PT_Jobs;