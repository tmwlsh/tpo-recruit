<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>

	<div class="jobs-page">

		<div class="page-header">
			<div class="container">
				<h1>View our latest job opportunities</h1>
			</div>
		</div>

		<div class="container">
			<div class="filters">
				<a href="#" class="filters__toggle">Filter jobs</a>

				<form action="<?php echo site_url('jobs'); ?>?ja=1" method="POST" class="filters__form" accept-charset="UTF-8">
					<?php
					foreach (array('job-location' => 'Location', 'job-category' => 'Discipline', 'job-salary' => 'Salary') as $taxonomy => $label) {
						$terms = get_terms(array(
							'taxonomy'   => $taxonomy,
						    'hide_empty' => true,
						) );

						if (!$terms) {
							continue;
						}
						?>

						<div class="filters__column">
							<h5 class="filters__title"><?php echo $label; ?></h5>

							<?php foreach ($terms as $term) { ?>
								<label class="filters__option">
									<input type="checkbox" name="<?php echo $taxonomy; ?>[]" value="<?php echo $term->term_id; ?>" />
									<p><?php echo $term->name; ?></p>
								</label><br />
							<?php } ?>
						</div>
						<?php
					}
					?>

			        <input type="submit" value="Filter" class="filters__submit" />
				</form>
			</div>

			<?php if ( have_posts() ): ?>
				<ul class="jobs filters__results clearfix">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php
						$jobCat = get_the_terms(get_the_id(), 'job-category');
						$category_style = (is_wp_error($jobCat)) ? 'default' : $jobCat[0]->slug;
						?>
						<li class="<?php echo $category_style; ?>-style">
							<?php $jobInfo = get_field('job_info'); ?>
							<a href="<?php the_permalink(); ?>">
								<h3><?php echo $jobInfo['job_title']; ?></h3>
								<?php $jobLoc = get_the_terms(get_the_ID(), 'job-location'); ?>
								<p class="location"><?php echo $jobLoc[0]->name; ?></p>
								<p><?php echo wp_trim_words( get_the_content(), 40, ' ... Read More' ); ?></p>
							</a>
							<div class="job-info">
								<div class="wage">
									<?php $jobSalary = get_the_terms(get_the_ID(), 'job-salary'); ?>
									<?php if ($jobSalary && !is_wp_error($jobSalary)) { ?>
										<p><?php echo $jobInfo['wage_bracket']; ?></p>
									<?php } ?>
								</div>
								<div class="date">
									<p><?php echo get_the_date('d/m/y'); ?></p>
								</div>
							</div>
						</li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div>

	</div>

<?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer' ) ); ?>