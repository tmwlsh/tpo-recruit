<?php
/**
 * Template Name: 01 - Home
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>

	<div class="homepage">
		<div class="home-content">
			<div class="container">
				<div class="home-markets">
					<h1>Hello! We're The<br>People Of: <span class="market-one">Digital</span><span class="market-two">Tech</span><span class="market-three">Agency</span><span class="market-four">Marketing</span></h1>
				</div>
			</div>
		</div>
		<div class="home-head-jobs">
			<div class="container clearfix">
				<div class="home-jobs-desc">
					<p><?php the_field('home_text'); ?></p>
				</div>
				<div class="home-jobs-col">
					<a href="<?php echo get_site_url(); ?>/jobs"><?php the_field('home_cta_text'); ?></a>
				</div>
				<?php $contact = get_field('contact', 'options'); ?>
				<a class="contact" href="tel:<?php echo $contact['phone_number']; ?>"><?php echo $contact['phone_number']; ?></a>
				<a class="contact" href="mailto:<?php echo $contact['email_address']; ?>"><?php echo $contact['email_address']; ?></a>
				<?php unset($contact); ?>
			</div>
		</div>
	</div>

	<?php get_template_part( 'modules/content-builder' ); ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer' ) ); ?>