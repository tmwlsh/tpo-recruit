(function ($) {

	$(document).ready(function(){

        function homepageCycle(){
            var divs = $('div.home-markets span').hide(),
            i = 0;

            (function cycle() {
                divs.eq(i).fadeIn(0)
                .delay(1000)
                .fadeOut(0, cycle);
                i = ++i % divs.length;
            })();
        };

        function bodyPadding(){
            $headerHeight = $("header").outerHeight();
            $("body").css("padding-top", $headerHeight);
        };

        bodyPadding();

        homepageCycle();

        $("div.hamb").click(function(){
            $(this).toggleClass("open");
            $("div.header-menu").toggleClass("open");
            $("header").toggleClass("dark");
        });

        setTimeout(function(){
            $("div.home-markets").fadeIn();
        }, 200);

        $(window).resize(function(){
            bodyPadding();
        });

        $('ul.jobs li').matchHeight();

        $('.filters__toggle').click(function (e) {
            e.preventDefault();

            $('.filters__form').slideToggle();
        })

        $('.filters__option input').change(function () {
            $('.filters__submit').trigger('click');
            $(this).parent().toggleClass("active-option");
        })

        $('.filters__submit').click(function (e) {
            e.preventDefault();

            $(this).prop('disabled', true);
            $('.filters__results').addClass('filters__results--processing');

            $.ajax({
                method : 'POST',
                url    : $('.filters__form').attr('action'),
                data   : $('.filters__form').serialize(),
            })
            .done(function (msg) {
                $('.filters__results').html(msg);

                $('ul.jobs li').matchHeight();

                $('.filters__submit').prop('disabled', false);
                $('.filters__results').removeClass('filters__results--processing');
            });

            return false;
        })

    });

}(jQuery));