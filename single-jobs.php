<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/html-header', 'parts/header' ) ); ?>

<div class="single-jobs-page">

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div class="page-header">
			<div class="container">
				<?php $jobInfo = get_field('job_info'); ?>
				<h1><?php echo $jobInfo['job_title']; ?></h1>
			</div>
		</div>

		<div class="main-content">
			<div class="container clearfix">
				<div class="left">
					<h2><?php echo $jobInfo['job_title']; ?></h2>
					<div class="job-copy">
						<?php the_content(); ?>
					</div>
				</div>
				<div class="right">
					<div class="right-row">
						<h3>Salary:<br/><?php echo $jobInfo['wage_bracket']; ?></h3>
					</div>
					<div class="right-row">
						<h3>Location:<br/><?php echo get_the_terms(get_the_id(), 'job-location')[0]->name; ?></h3>
					</div>
					<div class="right-row">
						<h3>Discipline:<br/><?php echo get_the_terms(get_the_id(), 'job-category')[0]->name; ?></h3>
					</div>
					<div class="right-row">
						<h3>Job Code:<br/><?php the_title(); ?></h3>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; ?>

</div>

<?php Starkers_Utilities::get_template_parts( array( 'parts/footer','parts/html-footer' ) ); ?>